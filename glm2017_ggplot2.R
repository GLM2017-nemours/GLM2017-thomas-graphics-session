# Ecole thématique GLM novembre 2017, CEREEP
# GLM course, November 2017

library(ggplot2)
# Reference site for documentation
http://ggplot2.tidyverse.org/reference/

# Update depending on input file location
setwd('/Users/tully/CNRS_ownCloud/Plasphen/partage')

list.files()
# Loading maturation.txt and explicitly specifying colclass to ensure that temperature is a factor and temp is numeric
maturation=read.table(file = "maturation.txt", dec='.', sep="\t", colClasses=c('factor','factor','factor', 'integer','numeric','factor', 'numeric'))

#maturation=subset(maturation, clone %in%c('HA','TO'))
#maturation$clone=as.factor(as.character(maturation$clone))

str(maturation)

#temperature
ggplot(aes(y=tailleponte, x=temperature), data=maturation)+
  theme_bw()+
  geom_point()

#temp
ggplot(aes(y=tailleponte, x=temp), data=maturation)+
  theme_bw()+
  geom_point()


ggplot(aes(y=tailleponte, x=temp), data=maturation)+
  theme_bw()+
  geom_jitter(width = 0.4, height = 0, alpha=0.5, size=2, col="salmon")+
  labs(title = "Clutch size", y="Fecundity at maturity", x="Temperature")
  

ggplot(aes(y=tailleponte, x=temp, shape=clone, color=clone), data=maturation)+
  theme_bw()+
  geom_smooth(se = T, method = "lm", col="red", lwd=1)+
  geom_jitter(width = 0.4, height = 0, alpha=0.5, size=2, col="salmon")+
  labs(title = "Clutch size", y="Fecundity at maturity", x="Temperature")

# Overlap so we add facet_grid to split data onto two panels
ggplot(aes(y=tailleponte, x=temp, color=clone), data=maturation)+
  theme_bw()+
  geom_smooth(se = T, method = "lm", lwd=1)+
  facet_grid(.~clone)+
  #facet_grid(clone~.)+
  geom_jitter(width = 0.4, height = 0, alpha=0.5, size=2)+
  labs(title = "Clutch size", y="Fecundity at maturity", x="Temperature")


ggplot(aes(y=tailleponte, x=age, color=temperature), data=maturation)+
  theme_bw()+
  geom_smooth(se = T, method = "lm", lwd=1)+
  facet_grid(temperature~clone)+
  geom_jitter(width = 0.4, height = 0, alpha=0.5, size=2)+
  labs(title = "Maturation", y="Fecundity at maturity", x="Age")




ggplot(aes(y=tailleponte, x=temp), data=maturation)+
  theme_bw()+
  geom_point(alpha=0.5, size=2 )+
  labs(title = "Clutch size", y="Fecundity at maturity", x="Temperature")




ggplot(aes(y=tailleponte, x=temp), data=maturation)+
  geom_point(alpha=0.5, size=2 )+
  labs(title = "Clutch size", y="Fecundity at maturity", x="Temperature")+
  theme_bw()+
  scale_x_continuous(breaks=unique(maturation$temp), limits = c(NA, 29))



ggplot(aes(y=tailleponte, x=temp), data=maturation)+
  geom_jitter(alpha=0.5, size=2, width = 0.2, height = 0 )+
  theme_bw()+
  labs(title = "Clutch size", y="Fecundity at maturity", x="Temperature")+
  scale_x_continuous(breaks=unique(maturation$temp), limits = c(NA, 29))


ggplot(aes(y=tailleponte, x=temp), data=maturation)+
  geom_jitter(alpha=0.5, size=2, width = 0.2, height = 0 )+
  theme_bw()+
  labs(title = "Clutch size", y="Fecundity at maturity", x="Temperature")+
  scale_x_continuous(breaks=unique(maturation$temp), limits = c(NA, 29))+
  geom_smooth()




ggplot(aes(y=tailleponte, x=temp, group=clone), data=maturation)+
  geom_jitter(alpha=0.5, size=2, width = 0.2, height = 0 )+
  labs(title = "Clutch size", y="Fecundity at maturity", x="Temperature")+
  scale_x_continuous(breaks=unique(maturation$temp), limits = c(NA, 29))+
  geom_smooth()






  


ggplot(aes(y=tailleponte, x=temp, group=clone, color=clone, shape=clone), data=maturation)+
  geom_jitter(alpha=0.5, size=2, width = 0.2, height = 0 , show.legend = F)+
  theme_bw()+
  labs(title = "Clutch size", y="Fecundity at maturity", x="Temperature")+
  scale_x_continuous(breaks=unique(maturation$temp), limits = c(NA, 29))+
  geom_smooth(show.legend = F)+
  facet_grid(.~clone)

#table(select(maturation, clone, temperature))



# Calculating average fertility rate for each clone:temperature
mod_tailleponte=glm(tailleponte~clone:as.factor(temperature)-1,maturation, family ="poisson")
summary(mod_tailleponte)

ci_tailleponte=data.frame(round(exp(confint(mod_tailleponte)), digits = 1));

colnames(ci_tailleponte)=c("inf_fec","sup_fec") # Renaming columns
ci_tailleponte$mean_fec=(exp(mod_tailleponte$coef)) # Adding average value
# Getting temperature and clutch size codes from row names in the model
ci_tailleponte$clone=as.factor(gsub("^.*clone(.*):.*$",'\\1', rownames(ci_tailleponte)))
ci_tailleponte$temperature=gsub("^.*temperature)(.*)$",'\\1', rownames(ci_tailleponte))

# Calculating average age to maturity for each clone:temperature
mod_agemat=glm(age~clone:as.factor(temperature)-1,maturation, family ="poisson")
ci_agemat=data.frame(round(exp(confint(mod_agemat)), digits = 1));
colnames(ci_agemat)=c("inf_age","sup_age")
ci_agemat$mean_age=as.numeric(exp(mod_agemat$coef))
# Getting temperature and clutch size codes from row names in the model
ci_agemat$clone=as.factor(gsub("^.*clone(.*):.*$",'\\1', rownames(ci_agemat)))
ci_agemat$temperature=gsub("^.*temperature)(.*)$",'\\1', rownames(ci_agemat))

# Merging the two tables
ci_agefec=merge(ci_agemat, ci_tailleponte)
ci_agefec$temp=as.numeric(ci_agefec$temperature)



FIG_FECMATURATION=
{
ggplot(data=maturation,aes(y=tailleponte, x=temp+c(-0.2,0.2)[as.numeric(clone)], group=clone,shape=clone, fill=clone))+
  theme_bw()+ 
  geom_point(alpha=0.5)+
  scale_x_continuous(breaks=unique(maturation$temp), limits = c(NA, 29))+
  scale_y_continuous( limits = c(0, NA))+
  theme(strip.background = element_blank())+
  labs(title = "Clutch size", y="Fecundity at maturity", x="Temperature")+
  geom_errorbar(data=ci_agefec,aes(ymin=inf_fec, ymax=sup_fec, y=mean_fec, x=temp+c(-0.2,0.2)[as.numeric(clone)]), colour=grey(0.5), alpha=0.8, width=.4, size=0.5)+
  geom_line(colour=grey(0.4), aes(x=temp+c(-0.2,0.2)[as.numeric(clone)], y=mean_fec),data=ci_agefec)+
 geom_point(data=ci_agefec, aes(x=temp+c(-0.2,0.2)[as.numeric(clone)], y=mean_fec, bg=clone), colour=grey(0.), size=3, alpha=1)+
  scale_shape_manual(values=c(21, 23))+
  scale_fill_manual(values=c(grey(1),grey(0.8)))+
  scale_linetype_manual(values=c(1,3))+
  scale_size_manual(values=c(1, 0.5))+
  theme(legend.position=c(.9, .9))
    }


FIG_AGEMAT=
{
  ggplot(data=maturation,aes(y=age, x=temp+c(-0.2,0.2)[as.numeric(clone)], group=clone,shape=clone, fill=clone))+
    theme_bw()+ 
    geom_point(alpha=0.5)+
    scale_x_continuous(breaks=unique(maturation$temp), limits = c(NA, 29))+
    scale_y_continuous( limits = c(0, NA))+
    theme(strip.background = element_blank())+
    labs(title = "Age at maturity", y="Age (nb. days)", x="Temperature")+
    geom_errorbar(data=ci_agefec,aes(ymin=inf_age, ymax=sup_age, y=mean_age, x=temp+c(-0.2,0.2)[as.numeric(clone)]), colour=grey(0.5), alpha=0.8, width=.4, size=0.5)+
    geom_line(colour=grey(0.4), aes(x=temp+c(-0.2,0.2)[as.numeric(clone)], y=mean_age),data=ci_agefec)+
    geom_point(data=ci_agefec, aes(x=temp+c(-0.2,0.2)[as.numeric(clone)], y=mean_age, bg=clone), colour=grey(0.), size=3, alpha=1)+
    scale_shape_manual(values=c(21, 23))+
    scale_fill_manual(values=c(grey(1),grey(0.8)))+
    scale_linetype_manual(values=c(1,3))+
    scale_size_manual(values=c(1, 0.5))+
    theme(legend.position=c(.9, .9))
}


# I create a small vector with nice-looking colors for temperatures.
# I choose the colors using the following website:
#http://colorbrewer2.org/#type=diverging&scheme=RdYlBu&n=5
cinqcouleurs=rev(c('#d7191c','#fdae61','#ffffbf','#abd9e9','#2c7bb6'))

# I create the FIG_FECAGEMATURATION object with all the code needed to generate
# a figure and age and fertility at maturity
FIG_FECAGEMATURATION=
{
  ggplot(data=maturation,aes(y=tailleponte, x=age, group=clone, fill=as.factor(temperature)))+#shape=clone,
    theme_bw()+ 
    scale_fill_manual(values = cinqcouleurs, guide = guide_legend(title = "Temperature"))+
    geom_point(alpha=0.5, size=2, show.legend = F, shape=23)+
    facet_grid(.~clone)+
    scale_x_log10(limits=c(8, 160),breaks=c(10,25,50,100))+scale_y_log10(limits=c(NA, 130), breaks=c(5,10,25,50,100))+
    theme(strip.background = element_blank())+
    labs(title = "Age and fecundity at maturity", y="Fecundity at maturity", x="Age at maturity")+
    geom_errorbar(data=ci_agefec,aes(ymin=inf_fec, ymax=sup_fec, y=mean_fec, x=mean_age, group=clone), colour=grey(0.5), alpha=0.5, width=.0, size=0.8)+
    geom_errorbarh(data=ci_agefec,aes(xmin=inf_age, xmax=sup_age, x=mean_age, y=mean_fec, group=clone), colour=grey(0.5), alpha=0.5,  size=0.5)+
    geom_path(colour=grey(0.4), aes(x=mean_age, y=mean_fec),data=ci_agefec, arrow=arrow(angle = 30, length = unit(0.15, "inches"),ends = "last", type = "open"))+
    geom_point(data=ci_agefec, aes(x=mean_age, y=mean_fec, bg=temperature), colour=grey(0.5), size=4, alpha=1, shape=21)+
     theme(legend.position="right")
}  
  
# A short snippet to demonstrate how to combine the three plots and prepare a
# nice pdf for the article...
# We need the cowplot package for that
library(cowplot)

# You can combine several plots into one figure with plot_grid
FIGFECetAge=plot_grid(FIG_FECMATURATION, FIG_AGEMAT, labels=c("A", "B"), ncol = 2, nrow = 1, align = "hv")

FIGFECetAge

# Again combining two other plots
FIGMATURATION=plot_grid(FIGFECetAge, FIG_FECAGEMATURATION, labels=c("", "C"), ncol = 1, nrow = 2, align = "")

# This enables to produce a nice looking pdf. Change base_height and
# base_aspect_ratio to adjust the size and shape of your figure
save_plot("FIGMATURATION.pdf", FIGMATURATION,base_height = 8,base_aspect_ratio = 1.2 )


FIG_FECMATURATION
FIG_FECAGEMATURATION
FIG_AGEMAT
?plot_grid

# That's it! Now to do it with your own data...
# Thomas Tully
# thomas.tully@upmc.fr
# @EspeSVT
